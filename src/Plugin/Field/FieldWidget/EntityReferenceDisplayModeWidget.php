<?php

namespace Drupal\drupal_next\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference_display_mode_widget' widget.
 *
 * @FieldWidget(
 *   id = "entity_reference_display_mode_widget",
 *   label = @Translation("Entity Reference with Display Mode Widget"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceDisplayModeWidget extends EntityReferenceAutocompleteWidget {

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs an EntityReferenceDisplayModeWidget instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $base_field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   The widget settings.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityDisplayRepositoryInterface $entity_display_repository, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_display.repository'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $targetEntityType = $this->fieldDefinition->getSetting('target_type');

    if ($targetEntityType !== 'view') {
      return $element;
    }

    $element['#prefix'] = '<div id="entity-reference-display-mode-wrapper-' . $delta . '">';
    $element['#suffix'] = '</div>';

    $element['target_id']['#ajax'] = [
      'callback' => [$this, 'updateDisplayModeOptionsAjaxCallback'],
      'wrapper' => 'entity-reference-display-mode-wrapper-' . $delta,
      'event' => 'change',
    ];

    $selectedDisplayMode = isset($items[$delta]->display_mode) ? $items[$delta]->display_mode : '';
    $selectedEntityId = $items[$delta]->target_id;

    if ($selectedEntityId) {
      // Load the view entity.
      $selected_view = Views::getView($selectedEntityId);


      $element['display_mode'] = [
        '#type' => 'select',
        '#title' => $this->t('Display Mode'),
        '#empty_option' => $this->t('- Select -'),
        '#options' => $this->getDisplayModeOptions($selected_view),
        '#default_value' => $selectedDisplayMode,
        '#description' => $this->t('Select the display mode for the referenced entity view.'),
      ];
    }

    return $element;
  }

    /**
     * Gets the display modes for a view.
     */
  public function getDisplayModeOptions($selected_view) {
    // Get the available display modes for the view entity.
    $available_display_modes = [];
    if ($selected_view && isset($selected_view->storage)) {
      foreach($selected_view->storage->get('display') as $display) {
        $available_display_modes[$display['id']] = $display['display_title'];
      }
    }

    return $available_display_modes;
  }

  /**
   * Ajax callback to update the options of the display mode select element.
   */
  public function updateDisplayModeOptionsAjaxCallback(array &$form, FormStateInterface $form_state) {
    $triggeringElement = $form_state->getTriggeringElement();
    $parents = array_slice($triggeringElement['#array_parents'], 0, -2);
    $element = NestedArray::getValue($form, $parents);
    $delta = $triggeringElement['#delta'];
    $wrapperId = 'entity-reference-display-mode-wrapper-' . $delta;
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#' . $wrapperId, $element));

    return $response;
  }

}
