<?php

namespace Drupal\drupal_next\Plugin\Field\FieldType;

use Drupal\breakpoint\BreakpointManagerInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\MapFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\Entity\File;
use Drupal\image\Entity\ImageStyle;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * Plugin implementation of the 'responsive_image_styles' field type.
 *
 * @FieldType(
 *   id = "responsive_image_styles",
 *   label = @Translation("Responsive iage styles field"),
 *   description = @Translation("Responsive image styles"),
 *   no_ui = TRUE,
 * )
 */
class ResponsiveImageStyleField extends MapFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $entity = $this->getEntity();

    if (!$entity instanceof File || substr($entity->getMimeType(), 0, 5) !== 'image') {
      return;
    }

    $responsive_image_styles = ResponsiveImageStyle::loadMultiple();
    $file_uri = $entity->getFileUri();

    // @TODO: Inject this dependency. Wait for issue: https://www.drupal.org/node/2053415.
    /** @var BreakpointManagerInterface $breakpointService */
    $breakpointService = \Drupal::service('breakpoint.manager');

    $responsive_image_styles_list = [];
    foreach ($responsive_image_styles as $responsive_image_style) {
      $image_style_mappings = $responsive_image_style->getImageStyleMappings();
      $breakpoint_group = $responsive_image_style->getBreakpointGroup();

      $breakpoints = $breakpointService->getBreakpointsByGroup($breakpoint_group);

      $image_styles = [];
      foreach($image_style_mappings as $image_style_item) {
        if ($image_style_item['image_mapping_type'] !== 'image_style') {
          continue;
        }

        $image_style = ImageStyle::load($image_style_item['image_mapping']);
        $image_style_url = $image_style->buildUrl($file_uri);
        $media_query = $breakpoints[$image_style_item['breakpoint_id']]->getMediaQuery();
        $image_styles[$image_style_item['image_mapping']] = ['url' => $image_style_url, 'media_query' => $media_query];
        $responsive_image_styles_list[$responsive_image_style->id()] = $image_styles;
      }


      if (count($image_styles) <= 0 ) {
          $fallback = $responsive_image_style->getFallbackImageStyle();
          $image_style = ImageStyle::load($fallback);
          $image_style_url = $image_style->buildUrl($file_uri);
          $image_styles[$fallback] = ['url' => $image_style_url, 'media_query' => ''];
          $responsive_image_styles_list[$fallback] = $image_styles;
      }
    }


    $this->list[] = $this->createItem(0, $responsive_image_styles_list);
  }

}
